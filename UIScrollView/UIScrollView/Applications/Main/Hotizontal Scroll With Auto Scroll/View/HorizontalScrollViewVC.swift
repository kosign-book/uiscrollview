//
//  HorizontalScrollViewVC.swift
//  UIScrollView
//
//  Created by Hong Kheatbung on 15/1/23.
//

import UIKit

class HorizontalScrollViewVC: UIViewController {

    @IBOutlet weak var topScrollView    : UIScrollView!
    
    //For store image to ScrollView
    let arrayImages  = ["image0", "image1", "image2", "image3", "image4", "image5"]
    
    // For content width of screen
    let contentWidth             = UIScreen.main.bounds.width
    
    // For content height
    let contentHeight            = 250.0
    
    //For content inset (x)
    let contentInsetX            = 20.0
    
    //For define current view offset (x)
    var CurrentViewOffsetX       = 0.0
   
    //For current index of image
    var CurrentIndex              = 0
    
    //For image number
    var imageNumber              = 0
    
    // For define time
    private var topTimer            : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        // Delegate Top ScrollView
        self.topScrollView.delegate    = self
        
        // Init Top ScrollView
        self.initScrollView()
        
        // Set timer for scroll view to start auto scroll
        self.topTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(currentImageIndex), userInfo: nil, repeats: true)
    }
    
    // For init ScrollView
    private func initScrollView() {
         
        // Set content size to ScrollView
        self.topScrollView.contentSize  = CGSize(width: self.contentWidth * Double(arrayImages.count), height: self.contentHeight)
        self.topScrollView.frame        = CGRect(x: 0, y: 44, width: self.contentWidth, height: self.contentHeight)
         
        while self.imageNumber <= self.arrayImages.count {
            
            // Top Scroll View
            let topFrame            = CGRect(x: self.CurrentViewOffsetX, y: 0, width: self.contentWidth, height: self.contentHeight) // Set frame
            let topImageView        = UIImageView(frame: topFrame)
            topImageView.image      = UIImage(named: "image\(self.imageNumber)")
            // Add subview into scrollView
            self.topScrollView.addSubview(topImageView)
            self.imageNumber                += 1
            self.CurrentViewOffsetX      += self.contentWidth
        }
        self.CurrentViewOffsetX      = 0
    }
     
    // Start auto scrolling
    private func startAutoScrollBanner(_ scrollView: UIScrollView) {
        
        // Clear old timer
        self.topTimer?.invalidate()
        
        // Set new timer
        self.topTimer       = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(currentImageIndex), userInfo: nil, repeats: true)
    }
    
    // Stop auto Top Scroll
    private func stopAutoTopScrollBanner(_ scrollView: UIScrollView? ) {
        self.topTimer?.invalidate()
    }
    
    @objc private func currentImageIndex() {
        // For check current image index
        if self.CurrentIndex < self.arrayImages.count - 1 {
            self.CurrentIndex                 += 1
            self.CurrentViewOffsetX          += self.contentWidth
            self.topScrollView.contentOffset    = CGPoint(x: self.CurrentViewOffsetX, y: 0)
        }
        else {
            self.CurrentIndex                = 0
            self.CurrentViewOffsetX          = 0
            self.topScrollView.contentOffset    = CGPoint(x: self.CurrentViewOffsetX, y: 0)
        }
    }
}

extension HorizontalScrollViewVC: UIScrollViewDelegate {
    
    // Start Auto Scroll when User's finished drag the scroll view
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.startAutoScrollBanner(scrollView)
    }
    
    // Stop Auto Scroll when User's start drag the collection
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
            self.stopAutoTopScrollBanner(scrollView)
    }
    
    //Tells the delegate when the user finishes scrolling the content.
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            let offSet  = self.topScrollView.contentOffset.x
            let index   = Double(round(offSet/contentWidth))
            let point   = CGPoint (x: CGFloat(index * contentWidth), y: targetContentOffset.pointee.y)
            targetContentOffset.pointee = point
    }

}

