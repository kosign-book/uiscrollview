//
//  ViewController.swift
//  UIScrollView
//
//  Created by Bun on 10/1/23.
//

import UIKit

class MainVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapBtnUpAndDown(_ sender: UIButton) {
        let vc = UIStoryboard(name: "InfinityScrollSB", bundle: nil).instantiateViewController(withIdentifier: "InfinityScrollVC") as! InfinityScrollVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapUpAndDown(_ sender: UIButton) {
        let vc = UIStoryboard(name: "HorizontalScrollViewSB", bundle: nil).instantiateViewController(withIdentifier: "HorizontalScrollViewVC") as! HorizontalScrollViewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

