//
//  ScrollViewUpAndDownVC.swift
//  UIScrollView
//
//  Created by Bun on 10/1/23.
//

import UIKit

class InfinityScrollVC: UIViewController {
    
    @IBOutlet weak var scrollView       : UIScrollView!
    
    // For content width of screen
    let contentWidth        = UIScreen.main.bounds.width
    
    // For content height of screen
    let contentHeight       = UIScreen.main.bounds.height * 3
    
    // For subView height
    let subviewHeight       = CGFloat(100)
    
    // For define current view offset (x,y)
    var currentViewOffset   = CGFloat(0)
    var alpha               = 0.1
    var rowNumber           = 0
     
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Delegate to scrollView
        self.scrollView.delegate        = self
        
        // Set contentSize to ScrollView
        self.scrollView.contentSize  = CGSize(width: self.contentWidth, height: self.contentHeight)
        
        // Initialize ScrollView and set view in ScrollView
        while self.currentViewOffset < self.contentHeight {
            // Set frame
            let frame   = CGRect(x: 0, y: self.currentViewOffset, width: self.contentWidth,                                                          height: self.subviewHeight)
            // Set frame to subview (UIView)
            let subview = UIView(frame: frame)
            
            // Set background to subview (UIView)
            subview.backgroundColor = UIColor(hue: self.alpha, saturation: 1, brightness: 1,                                       alpha: self.alpha)
            // Set label
            let label  = UILabel(frame: CGRect(x: frame.origin.x + 20, y: frame.origin.y + 25,                        width: 100, height: 50))
            label.textColor = .black
            label.text      = String(rowNumber)

            // Add subview into scrollView
            self.scrollView.addSubview(subview)
            
            // Add label into scrollView
            self.scrollView.addSubview(label)

            self.rowNumber += 1
            // Condition increase or reset alpha value
            self.alpha = self.alpha > 1 ? 0.3 : self.alpha + 0.2
            
            // Increase currentViewOffset
            self.currentViewOffset += self.subviewHeight
            self.scrollView.contentOffset.y += self.subviewHeight
        }
    }
}

// MARK: - UIScrollViewDelegate

extension InfinityScrollVC: UIScrollViewDelegate {
    
    // The scroll-view object in which the scrolling occurred.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Set frame
        let frame   = CGRect(x: 0, y: self.currentViewOffset, width: scrollView.contentSize.width, height: self.subviewHeight)
        // Set frame to subview (UIView)
        let subview = UIView(frame: frame)
        
        // Set background to subview (UIView)
        subview.backgroundColor = UIColor(hue: self.alpha, saturation: 1, brightness: 1, alpha: self.alpha)
         
        let label   = UILabel(frame: CGRect(x: frame.origin.x + 20, y: frame.origin.y + 25, width:              100, height: 50))
        label.textColor = .black
        label.text      = String(rowNumber)
   
        if scrollView.contentOffset.y > self.currentViewOffset - 800 {
            // Add subview into scrollView
            self.scrollView.addSubview(subview)
            
            // Add label into scrollView
            self.scrollView.addSubview(label)
            
            // Increase rowNumber for display count Total Views
            self.rowNumber                  += 1
            
            // Increase currentViewOffset
            self.currentViewOffset          += self.subviewHeight
            
            // Increase currentViewOffset
            scrollView.contentSize.height   += self.subviewHeight
        }
        // Condition increase or reset alpha value
        self.alpha = self.alpha > 1 ? 0.3 : self.alpha + 0.2
    }
}
